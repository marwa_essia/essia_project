
# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 

class article_fabricant(models.Model):
    _name = 'prod.tecdoc'
    
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('supplier id')
    ref_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    marque = fields.Char('Brand')
    prix_ht = fields.Char('Price HT')
    prix_ttc = fields.Char('Price TTC')
    art_id = fields.Many2one('tecodc.wizard',string='Designation')
   