# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class categorie_marque_veh(models.Model):
    _name = 'categorie.marque'
    name = fields.Char('Category')
    supplier_id = fields.Char('Supplier id')
    supplier = fields.Char('Supplier')
    marque = fields.Char('Brand')
  