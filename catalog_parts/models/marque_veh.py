# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class marque(models.Model):
    _name = 'marque.voiture'
    name = fields.Char('Brand')
    marque_id = fields.Char('Brand id')
    active = fields.Boolean(default= True)
    body_id = fields.Char("Body id")
    carroserie = fields.Boolean(string="test",default=True)
   