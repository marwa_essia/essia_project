# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class categorie(models.Model):
    _name = 'categorie.fabricant'
    name = fields.Char('Category')
    supplier_id = fields.Char('Supplier id')
    supplier = fields.Char('Supplier')
    test = fields.Boolean(default=True)
    article = fields.Char('Article')
    