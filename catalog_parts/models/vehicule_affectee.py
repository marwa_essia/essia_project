# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 


class veh_affecte(models.Model):
    _name = 'veh.aff'
    
    name = fields.Char('Brand')
    supplier = fields.Char('Supplier')
   