# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json



class article(models.Model):
    _name = 'article.stock'
    
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('Supplier id')
    ref_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    quantite = fields.Char('Qty On Hand')
    prix_ht = fields.Char('Price HT')
    prix_ttc = fields.Char('PriceTTC')
    entrepot = fields.Char('Warehouse')
    refer_id = fields.Many2one('search.reference', ondelete='cascade', string='Article')
    liste_id = fields.Many2one('search.fabricant', ondelete='cascade', string='Article')
    tec_article_id = fields.Char('TEECDOC Article Id')
    product_id = fields.Many2one('product.product', string='Product')
    client = fields.Many2one('res.partner', string='Customer')
    equiv_id = fields.Many2many('essia.ref_equiv')
    serach_nr = fields.Char('Search Number')    
  
    def devis(self):
        products = {}
        product_line = []
        ctx = self._context.copy()
        print ('--------context:',ctx)
        view_id = self.env.ref('sale.view_order_form').id
        products.update({'product_id': self.product_id.id,'designation':self.designation,'name':self.designation,'reference':self.ref_article,'fabricant':self.supplier, 'product_uom_qty':1, 'product_uom':self.product_id.uom_id.id,'list_price':self.prix_ht})
        product_line.append((0, 0, products))

        ctx.update({'default_order_line':product_line,'default_partner_id':self.client.id})
        print ('--------context:',ctx)
        return {

          'name': 'devis stock',
    
          'type': 'ir.actions.act_window',
    
          'res_model': 'sale.order',
    
          'view_mode': 'form',
    
          'view_type': 'form',
    
          'target': 'new',
       
          #'domain': domain,
          
          'context': ctx,
          #'view_id': self.env.ref('sale.view_order_form').id,
          #'res_id': self.id,

           } 
        
    @api.multi
    def equiv_ref(self): 
            res = {'domain': {'equiv_id': []}}
            view_id = self.env.ref('catalog_parts.equiv_stock_form_view').id
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/equivalentsref/"
            url2 = "http://"+url+path+self.tec_article_id+"/4"+"/6"
            print("-----URL" , url2)
            headers={'X-AUTH-TOKEN':token}
            myResponse = requests.get(url2, headers = headers)          
            if(myResponse.ok):
                jData = json.loads(myResponse.content.decode('utf-8'))
                print ("The response contains {0} properties".format(len(jData)))
                self._cr.execute("DELETE FROM essia_ref_equiv")  
                for data in jData:  
                    supplier = data['BRAND']
                    supplier_id = data['SUPPLIER_ID']
                    designation = data['DESIGNATION']
                    article_id = data['ARTICLE_ID'] 
                    reference = data['DISPLAY_NUMBER'] 
                    print(reference)
                    print(data)
                    search_product_vals=({               'supplier':supplier,
                                                         'tec_article_id':data['ARTICLE_ID'] ,
                                                         'designation':designation,
                                                         'name':designation+"["+reference+"]",
                                                         'ref_article':reference,}) 
                    equiv_id=self.env['essia.ref_equiv'].create(search_product_vals)
                    print ('-------------equiv_id_created:',equiv_id) 
                    print('------------ID :' , self.equiv_id.supplier)
                    
            print("---------------------------partie Références alternatives-----------------") 
                   
            path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/alternatifref/"
            url2 = "http://"+url+path+self.serach_nr+"/6/"+self.supplier
            print("-----URL" , url2)
            headers={'X-AUTH-TOKEN':token}
            myResponse = requests.get(url2, headers = headers)            
            if(myResponse.ok):
                jData = json.loads(myResponse.content.decode('utf-8'))
                print ("The response contains {0} properties".format(len(jData)))
    #             self._cr.execute("DELETE FROM essia_ref_equiv")  
                for data in jData:  
                    supplier_alt = data['SUPPLIER']
    #                 supplier_id_alt = data['SUPPLIER_ID']
                    designation_alt = data['DESIGNATION']
                    article_id_alt = data['ARTICLE_ID'] 
                    reference_alt = data['Refrence'] 
                    print(reference)
                    print(data)
                    alternatif_id=self.env['essia.ref_equiv'].search([('ref_article', '=',reference_alt),('designation','=',designation_alt),('supplier','=',supplier_alt)])
                  
                    if not alternatif_id:
                        search_ref_alternatif=({
                                                             'supplier':supplier_alt,
                                                             'tec_article_id':data['ARTICLE_ID'] ,
                                                             'designation':designation,
                                                             'name':designation+"["+reference+"]",
                                                             'ref_article':reference_alt,})  
                        equiv_id=self.env['essia.ref_equiv'].create(search_ref_alternatif)
                        print ('-------------equiv_id_created:',equiv_id)
                        print('------------ID :' , self.equiv_id.supplier)  

            return {
          
                                'name': ('Références Equivalentes'),
                              
                                'type': 'ir.actions.act_window',
                              
                                'res_model': 'article.stock',
                              
                                'view_mode': 'form',
                              
                                'view_type': 'form',
                              
                                'target': 'new',
                                 
                                'view_id': view_id,
                        #         'res_id': self.id,
    #                             'context':ctx,
    #  
                            } 
   
        
        
       
        