# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 


class article_fabricant(models.Model):
    _name = 'fabricant.stock'
    
    supplier = fields.Char('Supplier')
    ref_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    quantite = fields.Char('Quantity')
    prix_ht = fields.Char('Price HT')
    prix_ttc = fields.Char('Price TTC')
    supplier_id = fields.Char('Supplier id')
    art_id = fields.Char('TEECDOC Article Id')
    product_id = fields.Many2one('product.product', string='Product')
    fa_id = fields.Many2one('search.fabricant', ondelete='cascade', string='Article')

   