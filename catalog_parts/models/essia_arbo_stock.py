# -*- coding: utf-8 -*-
from odoo import models, fields , api

import requests

import json

class arbo_stock(models.Model):
    
    _name = 'essia.arbo_stock'
    name = fields.Char('Description')
    active = fields.Boolean(default= True)
    motorisation_id=fields.Char('Motorisation type ID')
    str_id= fields.Char('Str_id')
   