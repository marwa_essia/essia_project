# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 


class article_fabricant(models.Model):
    _name = 'marq.tecdoc'
    
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('Supplier id')
    art_id = fields.Char('art id')
    ref_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    marque = fields.Char('Brand')
    marq_id = fields.Many2one('search.fabricant', ondelete='cascade')