# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 


class herit_produit(models.Model):
    _inherit = 'product.template'
    designation = fields.Char('Designation')
    #article_id =  fields.Char(string='article id',invisible ="1")
    supplier_id = fields.Char('Supplier id')
    desig_distributeur = fields.Char('Distributor designation', compute='_compute_desig_distributeur',
        inverse='_set_desig_distributeur', store=True)
    marque = fields.Char('Brand')
    supplier = fields.Char(
        'Supplier', compute='_compute_supplier',
        inverse='_set_supplier', store=True)
    article_id = fields.Char(
        'Article id', compute='_compute_article_id',
        inverse='_set_article_id', store=True)
 
    
    @api.depends('product_variant_ids', 'product_variant_ids.supplier')
    def _compute_supplier(self):
        unique_variants = self.filtered(lambda template: len(template.product_variant_ids) == 1)
        for template in unique_variants:
            template.supplier = template.product_variant_ids.supplier
        for template in (self - unique_variants):
            template.supplier = ''

    @api.one
    def _set_supplier(self):
        if len(self.product_variant_ids) == 1:
            self.product_variant_ids.supplier = self.supplier
    @api.depends('product_variant_ids', 'product_variant_ids.desig_distributeur')
    def _compute_desig_distributeur(self):
        unique_variants = self.filtered(lambda template: len(template.product_variant_ids) == 1)
        for template in unique_variants:
            template.desig_distributeur = template.product_variant_ids.desig_distributeur
        for template in (self - unique_variants):
            template.desig_distributeur = ''

    @api.one
    def _set_desig_distributeur(self):
        if len(self.product_variant_ids) == 1:
            self.product_variant_ids.desig_distributeur = self.desig_distributeur
    @api.depends('product_variant_ids', 'product_variant_ids.article_id')
    def _compute_article_id(self):
        unique_variants = self.filtered(lambda template: len(template.product_variant_ids) == 1)
        for template in unique_variants:
            template.article_id = template.product_variant_ids.article_id
        for template in (self - unique_variants):
            template.article_id = ''

    @api.one
    def _set_article_id(self):
        if len(self.product_variant_ids) == 1:
            self.product_variant_ids.article_id = self.article_id
    