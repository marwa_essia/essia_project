
# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 

class article_fabricant(models.Model):
    _name = 'many.art'
    
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('Supplier id')
    ref_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    marque = fields.Char('Brand')
    prix_ht = fields.Char('Price HT')
    prix_ttc = fields.Char('Price TTC')
    product_exist = fields.Boolean('Product Exist')
    article_id = fields.Char('Article id')
    @api.multi
    def stock(self):
        ctx = self._context.copy()
        active_id = self.id
        print ('----active_id:',active_id)
        ctx.update({'default_default_code': self.ref_article,'default_type':'product','default_supplier': self.supplier,'default_designation': self.designation,'default_supplier_id': self.supplier_id,'default_name': self.designation,'default_marque': self.marque,'default_article_id': self.article_id})
        view_id = self.env.ref('product.product_normal_form_view').id
        model = 'product.product'
        return {
        'name': _('stock'),
        'type': 'ir.actions.act_window',
        'view_mode': 'form',
        'view_type': 'form',
        'res_model': model,
        'context': ctx,
        'target': 'new', 
        }        
