# -*- coding: utf-8 -*-
from . import connexion

# list of models for search by reference
from . import search_catalogue
from . import herit_devis
from . import herit_produit
from . import article_stock
from . import Recherche_tecdoc
from . import entrepot_ref
from . import supplier_filter
from . import essia_ref_equiv
# list of models for search by supplier

from . import marque_veh
from . import search_fabricant
from . import fabricant_tecdoc
from . import article_stock_fabricant
from . import categorie_tecdoc
from . import herit_warehouse
from . import categorie_fabricant
from . import marque_fabricant
from . import tecdoc_categ_fab
from . import tecdoc_marque_art
from . import categorie_stock
from . import marque_categorie
from . import marque_stock_veh
from . import mar_cat_stock
from . import stock_tab
from . import article_tecdoc_popup
from . import tecdoc_manyart
from . import categ_tecdoc_ref
from . import categ_stck_rech
from . import marque_entrep
from . import marque_duplication
from . import categorie_dupl
from . import categorie_article_stock
from . import categorie_marque_stock
from . import vehicule_affectee

# list of models for search by supplier

from . import search_by_vehicule
from . import motorisation_tec_doc
from . import modele_tec_doc
from . import arbo_childs_tecdoc
from . import essia_arboresencetecdoc
from . import essia_arbo_stock
from . import crosserietecdoc
from . import essia_articles_arbo_stock
from . import essia_articles_stock
from . import essia_articles_tec
from . import marquetecdoc


# others models

from . import sale
from . import herit_res_partner
from . import herit_product_template
from . import herit_account_invoice_line


