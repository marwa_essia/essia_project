# -*- coding: utf-8 -*-
from odoo import models, fields , api ,_ , exceptions
import requests
import json



class essia_articles_stock(models.Model):
    
    _name = 'essia.articlesstock'  
    name= fields.Char('Description')  
    supplier = fields.Char('Supplier')
    ref_tecdoc_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    supplier= fields.Char('Supplier')
    desig_tecdoc= fields.Char('Desig_tecdoc')
    prix_ht = fields.Char('Price HT')
    prix_ttc = fields.Char('Price TTC')
    qte = fields.Char('Qty')
    parent= fields.Char('Parent')
    @api.multi
    def stock_devis(self): 
        products = {}
        product_line = []
        ctx = self._context.copy()
        view_id = self.env.ref('sale.view_order_form').id
        products.update({'designation':self.designation,'reference':self.ref_tecdoc_article,'fabricant':self.supplier})
        product_line.append((0, 0, products))
        ctx.update({'default_order_line':product_line})
        print ('--------context:',ctx)
        return {
    
            'name': 'devis stock',
        
            'type': 'ir.actions.act_window',
        
            'res_model': 'sale.order',
        
            'view_mode': 'form',
        
            'view_type': 'form',
        
            'target': 'new',
    #         'view_id': view_id,
    #         'res_id': self.id,
            'context':ctx,

      } 
        