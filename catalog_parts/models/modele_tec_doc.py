# -*- coding: utf-8 -*-
from odoo import api, fields, models, _ 
import requests
import json

class modele_tec_doc(models.Model):
    _name = 'essia.modele_tecdoc'
  
    name = fields.Char('Model')
    model_id = fields.Char('Model id')
    active = fields.Boolean(default= True)
    test = fields.Boolean(default= True)
    carrosserie_id = fields.Char("Body id")
    marque_id = fields.Char("Brand_id")
      
   
   