# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class categorie(models.Model):
    _name = 'categorie.dupl'
    name = fields.Char('Category')
    supplier = fields.Char('Supplier ')
    supplier_id = fields.Char('Supplier id')
    test = fields.Boolean(default=True)
