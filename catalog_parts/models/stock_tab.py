# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 


class article_fabricant(models.Model):
    _name = 'prod.stock'
    
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('Supplier id')
    art_id = fields.Char('art id')
    ref_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    prod_id = fields.Many2one('search.fabricant', ondelete='cascade')
    marque = fields.Char('Brand')
    prix_ht = fields.Char('Price HT')
    prix_ttc = fields.Char('Price TTC')
    qte = fields.Char('Qty')
    categorie_stock = fields.Many2one('categorie.stock',string='category')

    @api.multi 
    def categorie_devis(self): 
      products = {}
      product_line = []
      ctx = self._context.copy()
      print("designation ,reference: ",(self.categorie_stock.name ,  self.categorie_stock.supplier_id)) 
      print ('--------context:',ctx)
      view_id = self.env.ref('sale.view_order_form').id
      products.update({'designation':self.designation,'reference':self.ref_article,'fabricant':self.supplier})
      product_line.append((0, 0, products))
      ctx.update({'default_order_line':product_line})
      print ('--------context:',ctx)
      return {

        'name': 'devis stock',
    
        'type': 'ir.actions.act_window',
    
        'res_model': 'sale.order',
    
        'view_mode': 'form',
    
        'view_type': 'form',
    
        'target': 'new',
#         'view_id': view_id,
#         'res_id': self.id,
        'context':ctx,

      }                   
   