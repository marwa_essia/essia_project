# -*- coding: utf-8 -*-
from odoo import models, fields , api ,_ , exceptions
import requests
import json



class essia_articles_tecdoc(models.Model):
    
    _name = 'essia.articlestecdoc'  
    name= fields.Char('Description')  
    supplier = fields.Char('Supplier')
    ref_tecdoc_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    tecdoc_id = fields.Many2one('essia.searchbyvehicule', ondelete='cascade', string='tecdoc')
    tecdoc_article_id = fields.Char('TEECDOC Article Id')
    product_exist = fields.Boolean('Product exist')
    supplier_id = fields.Char('Supplier_id')
    parent_stock = fields.Char('Parent')
    type_mot_id = fields.Char('Type_mot_id')
    desig_tecdoc= fields.Char('Desig_tecdoc')
    article_id = fields.Char('Article_id')
    parent_stock = fields.Char('parent')
    type_mot_id = fields.Char('type_mot_id')
    desig_tecdoc= fields.Char('desig_tecdoc')
    str_id_stock = fields.Char('str_id_stock')
    article_id= fields.Char('article_id')
    @api.multi
    def ajout_stock(self):
        ctx = self._context.copy()
        active_id = self.id
        print ('----active_id:',active_id)
        ctx.update({'default_default_code': self.ref_tecdoc_article,'default_type':'product','default_supplier': self.supplier,'default_designation': self.designation,'default_name': self.designation,'default_parent_stock':self.parent_stock,'default_type_mot_id':self.type_mot_id,'default_desig_tecdoc':self.desig_tecdoc, 'default_str_id_stock':self.str_id_stock, 'default_article_id':self.article_id })
        print('-------context:',ctx)
        view_id = self.env.ref('product.product_normal_form_view').id
        model = 'product.product'
        return {
        'name': _('Ajouter article'),
        'type': 'ir.actions.act_window',
        'view_mode': 'form',
        'view_type': 'form',
        'res_model': model,
        'context': ctx,
        'target': 'new', 
        }
            