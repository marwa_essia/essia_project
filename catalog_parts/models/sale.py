# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    
    fabricant = fields.Char('Supplier')
    reference = fields.Char('Reference')
    designation = fields.Char('Designation')
    product = fields.Many2one('product.product',string = 'Product')
    @api.multi
    @api.onchange('product_id')
    def product_id_fabricant(self):
        if self.product_id:
            self.fabricant = self.product_id.supplier
    