# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class categorie(models.Model):
    _name = 'marque.stck'
    name = fields.Char('Category')
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('Supplier id')
    ref_article = fields.Char('Reference')
    marque = fields.Char('Brand')
    designation = fields.Char('Designation')
    prix_ht = fields.Char('Price HT')
    prix_ttc = fields.Char('Price TTC')
    qte = fields.Char('Qty On Hand')
    product_id = fields.Many2one('product.product', string='Product')
    client = fields.Many2one('res.partner', string='Customer')

    def stock_devis(self): 
      products = {}
      product_line = []
      ctx = self._context.copy()
      view_id = self.env.ref('sale.view_order_form').id
      products.update({'product_id': self.product_id.id,'name':self.designation,'designation':self.designation,'reference':self.ref_article,'fabricant':self.supplier, 'product_uom_qty':1, 'product_uom':self.product_id.uom_id.id,'list_price':self.prix_ht})
      product_line.append((0, 0, products))
      ctx.update({'default_order_line':product_line,'default_partner_id':self.client.id})
      print ('--------context:',ctx)
      return {

        'name': 'devis stock',
    
        'type': 'ir.actions.act_window',
    
        'res_model': 'sale.order',
    
        'view_mode': 'form',
    
        'view_type': 'form',
    
        'target': 'new',
#         'view_id': view_id,
#         'res_id': self.id,
        'context':ctx,

      } 