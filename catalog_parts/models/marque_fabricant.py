# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class marque(models.Model):
    _name = 'marque.fabricant'
    name = fields.Char('Brand')
    test = fields.Boolean(default=True)
    marque = fields.Char('Brand')
    supplier_id = fields.Char('Supplier id')
    supplier = fields.Char('Supplier')
