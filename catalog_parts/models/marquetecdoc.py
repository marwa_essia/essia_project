# -*- coding: utf-8 -*-
from odoo import models, fields , api

import requests

import json


class marquetecdoc(models.Model):
    
    _name = 'essia.marquetecdoc'
   

    name = fields.Char('Brand')
    marque_id = fields.Char('TecDoc Brand ID')
    active = fields.Boolean (default= True)
    crosserie = fields.Boolean('Body')
    body_id = fields.Char('Body ID')
    
    