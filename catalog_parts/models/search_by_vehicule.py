# -*- coding: utf-8 -*-
from odoo import models, fields , api ,_ , exceptions
import requests
import json

class search_by_vehicule(models.Model): 
    _name = 'essia.searchbyvehicule' 
    _order = "date desc, id desc"   
    name =   fields.Char('Name')      
    marque_id = fields.Many2one('essia.marquetecdoc',string='Brand')
    crosserie_id= fields.Many2one('essia.crosserietecdoc',string='Body')
    modele_id = fields.Many2one('essia.modele_tecdoc',string='Model')
    motorisation_id = fields.Many2one('essia.motorisation_tec_doc',string='Engine')
    str_text = fields.Char('Tree')
    arboresence_id = fields.Many2one('essia.arboresencetecdoc', string='Arboresence')
    arboresence_child_id=fields.Many2one('essia.arbo_childs_tecdoc', string='Arboresence fils')
    parent_id=fields.Many2one('essia.arboresencetecdoc', string='parent_id')
    field_name = fields.Many2many('essia.arboresencetecdoc', string="4") 
    arbo1=fields.Html('arboresence parent')
    arbo_tec_doc=fields.Html('arbo',readonly=True)   
    arbo_par=fields.Text('Arboresence Parente')
    moto_exist=fields.Boolean('motorisation existe')
    lengthJdata=fields.Char('length')
    descendants=fields.Char('descendants')
    arbo_stock_id= fields.Many2one('essia.arbo_stock', string='aboresence stock')
    articles_arbo_stock_id= fields.Many2one('essia.articles_arbo_stock', string='aricles stock')
    article_id = fields.Many2one('essia.articlestecdoc', string='Articles')
    client_id = fields.Many2one('res.partner', string='Customer')
    date = fields.Datetime('Date', required=True, readonly=True, select=True, default=fields.datetime.now())
    user = fields.Many2one('res.users','User', default=lambda self: self.env.user)
    @api.multi
    def search_by_vehicule(self): 
        tecdoc_obj = self.env['connexion.server']
        tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
        url = tecdoc_id.url
        token = tecdoc_id.token
        path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/marques/1"
        url1 = "http://"+url+path
        headers={'X-AUTH-TOKEN':token}
        myResponse = requests.get(url1, headers = headers)
        if(myResponse.ok):
            jData = json.loads(myResponse.content.decode('utf-8'))
            for data in jData: 
                marque_id = data['mfa_id']
                print(data['mfa_brand'])
                marq_id = self.env['essia.marquetecdoc'].search([('active', '=',True),('marque_id' ,'=', marque_id)])
                if not marq_id:
                    marque_vals = { 'name':data['mfa_brand'],
                                    'marque_id':data['mfa_id'],}
                    self.env['essia.marquetecdoc'].create(marque_vals)    
        return True

    @api.multi
    @api.onchange('marque_id')
    def onchange_marque_id(self):
        print ('---------yes------------')
        res = {'domain': {'crosserie_id': []}}
        self.crosserie_id = [(5, 0, 0)]
        self.modele_id= [(5, 0, 0)]
        self.motorisation_id= [(5, 0, 0)]
        self.arbo_stock_id = [(5, 0, 0)]
        self.arbo_tec_doc = [(5, 0, 0)]
        self.arboresence_id = [(5, 0, 0)]
        self.articles_arbo_stock_id = [(5, 0, 0)]
        self.arboresence_child_id = [(5, 0, 0)]
        if (self.marque_id):
            self.name =str(self.marque_id.name)+"/"
            print("----Marque ID :",self.marque_id.marque_id,"----Nom marque: ",self.marque_id.name)  
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/bodys/"
            urlcross = "http://"+url+path+self.marque_id.marque_id
            headers={'X-AUTH-TOKEN':token}
            myResponsecross = requests.get(urlcross, headers = headers)
            if(myResponsecross.ok):
                jData = json.loads(myResponsecross.content.decode('utf-8'))
                print ("The response contains {0} properties".format(len(jData)))
                for data in jData: 
                        body_id = data['vhbId']
                        print("------Id Crosserie : ------",body_id ,"Nom Crosserie :" ,data['vhbName'])
                        cross_id = self.env['essia.crosserietecdoc'].search([('active', '=',True),('crosserie_id','=',body_id),('marque_id', '=',self.marque_id.marque_id)])
                        print ('-------------cross_id_exist:',cross_id)  
                        if not cross_id:
                            body_vals = { 'name':data['vhbName'],
                                          'crosserie_id':data['vhbId'],
                                          'marque_id':self.marque_id.marque_id, }
                            cross_id = self.env['essia.crosserietecdoc'].create(body_vals)  
                            print ('-------------cross_id_created:',cross_id)
                        res['domain']['crosserie_id'] =  [('marque_id', '=', self.marque_id.marque_id)] 
        else : print("-----------marque null----------") 
        self.modele_id= [(5, 0, 0)]        
        return res

    @api.multi
    @api.onchange('crosserie_id')
    def onchange_carrosserie_id(self):
        print ('---------yes-----------')
        res = {'domain': {'modele_id': []}}
        self.modele_id= [(5, 0, 0)]
        self.motorisation_id= [(5, 0, 0)]
        
        if self.crosserie_id :
            self.name =self.name+str(self.crosserie_id.name)+"/"
            print('---Marque :',self.marque_id.name,'---IDMarque :', self.marque_id.marque_id ,"---crosserie :" ,self.crosserie_id.name, "---Id Crosserie :",self.crosserie_id.crosserie_id)
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path1 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/models/"
            urlmodel = "http://"+url+path1+self.marque_id.marque_id+"/"+self.crosserie_id.crosserie_id
            print(urlmodel)
            headers={'X-AUTH-TOKEN':token}
            myResponsemodel = requests.get(urlmodel, headers = headers)
            if(myResponsemodel.ok):
                jData = json.loads(myResponsemodel.content.decode('utf-8'))
                print ("The response contains {0} properties".format(len(jData)))
                for data in jData: 
                    print(data)
                    model_id = data['vhModId']
                    model_name = data['vhModName']
                    model_date_debut= str(data['vhStartDate'])
                    print('----date de début :',model_date_debut)
                    mod_id = self.env['essia.modele_tecdoc'].search([('active', '=',True),('model_id', '=', model_id)])#,('marque_id', '=',self.marque_id.marque_id),('carrosserie_id', '=', self.crosserie_id.crosserie_id)])
                    if not mod_id:
                        mod_vals = {   'name':data['vhModName'],#+"("+model_date_debut_res+")",#str(data['vhStartDate'])+")",#"("+str(data['vhStartDate'])+str(data['vhEndDate'])+")",#model_date_debut_res,#+res_dat_fin,
                                       'model_id':data['vhModId'] ,
                                       'carrosserie_id':self.crosserie_id.crosserie_id,
                                       'marque_id':self.marque_id.marque_id 
                                       }
                        mod_id = self.env['essia.modele_tecdoc'].create(mod_vals)
                        print ('-------------mod_id_created:',mod_id)
            res['domain']['modele_id'] = [('carrosserie_id', '=', self.crosserie_id.crosserie_id),('marque_id', '=',self.marque_id.marque_id)]
        else:      
            print("------crosserie et marque sont nulles-------")
        return res
    @api.multi
    @api.onchange('modele_id')
    def onchange_modele_id(self):
        print("----------yes---------") 
        res = {'domain': {'motorisation_id': []}}
        self.motorisation_id= [(5, 0, 0)]
        if self.modele_id :
           self.name = self.name+str(self.modele_id.name)+"/"
           print("---Modele : ", self.modele_id.name,"---ID Modele: ",self.modele_id.model_id)
           tecdoc_obj = self.env['connexion.server']
           tecdoc_id = tecdoc_obj.search([('active', '=', True)])
           url = tecdoc_id.url
           token = tecdoc_id.token
           path2 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/types/"
           url_motorisation = "http://"+url+path2+self.modele_id.model_id+"/6"
           print("url : ", url_motorisation)
           headers={'X-AUTH-TOKEN':token}
           myResponse_motorisation = requests.get(url_motorisation, headers = headers)
           if(myResponse_motorisation.ok):
                jData = json.loads(myResponse_motorisation.content.decode('utf-8'))
                print ("The response contains {0} properties".format(len(jData)))
                self._cr.execute("DELETE FROM essia_motorisation_tec_doc")
                for data in jData: 
                    print(data)
                    typ_id = data['typId']
                    engine_litre = data['ENGINE_LITRE']
                    typ_engine = data['TYP_ENGINE_DES_TEXT']
                    mot_id = self.env['essia.motorisation_tec_doc'].search([('active', '=',True),('typ_id', '=', typ_id),('model_tec_id','=',self.modele_id.model_id)])
                    if not mot_id:
                        mot_vals = {   'name':data['TYP_FUEL_DES_TEXT']+":"+str(data['ENGINE_LITRE'])+" "+data['TYP_CDS_TEXT']+"-"+data['ENGINE_VALVES']+"V"+"-"+str(data['typHpFrom'])+"CH"+"-"+str(data['typCcm'])+"CC",
                                       'typ_id':data['typId'],
                                       'model_tec_id':self.modele_id.model_id }
                        mot_id=self.env['essia.motorisation_tec_doc'].create(mot_vals)
                        print ('-------------mod_id_created:',mot_id)
                    res['domain']['motorisation_id'] = [('model_tec_id','=',self.modele_id.model_id)]
        else:      
            print("-------Modéle is null----------")
        return res 
    
    @api.multi
    @api.onchange('motorisation_id','marque_id')
    def onchange_motorisation_id(self):
        print("----------yes---------") 
        self.arboresence_id = [(5, 0, 0)]
        self.arboresence_child_id = [(5, 0, 0)]
        res = {'domain': {'arboresence_id': []}}
        res1 = {'domain': {'arboresence_id': []}}
        res2 = {'domain': {'arboresence_id': []}}
        if  self.motorisation_id and self.marque_id :
            self.name = self.name+str(self.motorisation_id.name)
            print('----motorisation_id : ',self.motorisation_id.typ_id,"----Modéle :", self.modele_id.name , "---Marque :", self.marque_id.name)
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/sectionbyparent/"
            url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/10001/6"
            print('---URL : ',url1)
            headers={'X-AUTH-TOKEN':token}
            myResponse3 = requests.get(url1, headers = headers)
            if(myResponse3.ok):
                jData = json.loads(myResponse3.content.decode('utf-8'))
                print ("The response contains {0} properties".format(len(jData))) 
                lengthJdata=int(format(len(jData)))
                print("-----longueur JData-----",lengthJdata)
                self._cr.execute("DELETE FROM essia_arboresencetecdoc") 
                if lengthJdata == 0 :
                    #raise exceptions.ValidationError('No Arboresence!')
#                     _('You plan to sell %s quantity but you only have %s available in %s warehouse.') % \
#                      (self.qty, self.product_id.virtual_available, self.order_id.warehouse_id.name)
                    message = _('Cette voiture du marque : %s , Modéle : %s et du  type motorisation : %s ne  posséde pas une arboresence')% \
                               (self.marque_id.name,self.modele_id.name, self.motorisation_id.name)
                    mess= {
                                        'title': _('No arboresence!'),
                                        'message' : message
                                    }
                    return {'warning': mess}
                else:
                    for data in jData:
                        text_id = data['texId']
                        str_text = data['STR_DES_TEXT']
                        str_id = str (data ['strId'])
                        descendants = data ['DESCENDANTS']
                        print(data)
                        arb_id = self.env['essia.arboresencetecdoc'].search([('active', '=',True),('typ_motorisation_id','=',self.motorisation_id.typ_id),('text_id','=', text_id)])
                        if not arb_id:               
                            arb_vals = {   'name':data['STR_DES_TEXT'],
                                            'str_id':data['strId'],
                                            'str_id_parent':data['strIdParent'],
                                            'text_id':data['texId'],
                                            'typ_motorisation_id':self.motorisation_id.typ_id,
                                            'lengthJdata':lengthJdata}
                            arb_id = self.env['essia.arboresencetecdoc'].create(arb_vals)
                            print ('-----------arb_id_created:',arb_id) 
                    res['domain']['arboresence_id'] = [('typ_motorisation_id','=',self.motorisation_id.typ_id)]                                                                                     
            path4 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/sectionbyparent/"
            url4 = "http://"+url+path4+self.motorisation_id.typ_id+"/10001/6"
            print(url4)
            myResponse4 = requests.get(url4, headers = headers)
            if(myResponse4.ok):
                print(url4) 
                jData = json.loads(myResponse4.content.decode('utf-8'))
                self._cr.execute("DELETE FROM essia_arbo_stock")
#             self._cr.execute("DELETE FROM marque_entrep")
                for data in jData:
                    text_id = data['texId']
                    parent_tecdoc = data['STR_DES_TEXT']
                    str_id = str (data ['strId'])
                    descendants = data ['DESCENDANTS']
                    print(parent_tecdoc)
                    arbo_stock = self.env['product.product'].search([('parent_stock', '=', parent_tecdoc),('type_mot_id', '=', self.motorisation_id.typ_id)])
#                     print(arbo_stock)
                    if arbo_stock:   
                        arbo_stock_vals = {    'name':data['STR_DES_TEXT'],
                                               'motorisation_id':self.motorisation_id.typ_id,
                                               'str_id':data['strId']}          
                        self.env['essia.arbo_stock'].create(arbo_stock_vals)
                        print('-------arbo stock is created:',arbo_stock)
                        res['domain']['arbo_stock_id'] =  [('motorisation_id', '=', self.motorisation_id.typ_id)]  
                        print('------res : ',res)                           
        else:
            print("-------------type motorisation vide")   
        return res 
    
    @api.multi
    @api.onchange('arboresence_id')
    def onchange_arboresence_id(self):
        print("----------yes---------") 
        resarbo = {'domain': {'arboresence_child_id': []}}   
        if  self.arboresence_id :
                print('----motorisation_id : ',self.arboresence_id.typ_motorisation_id,"----ID Parent :",self.arboresence_id.str_id)
                tecdoc_obj = self.env['connexion.server']
                tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
                url = tecdoc_id.url
                token = tecdoc_id.token
                path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/sectionbyparent/"
                url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/"+self.arboresence_id.str_id+"/6"
                print('---URL : ',url1)
                headers={'X-AUTH-TOKEN':token}
                myResponse3 = requests.get(url1, headers = headers)
                if(myResponse3.ok):
                    jData = json.loads(myResponse3.content.decode('utf-8'))
                    print ("The response contains {0} properties".format(len(jData)))
                    lengthJdata=int(format(len(jData)))
                    print("-----longueur JData-----",lengthJdata)
                    self._cr.execute("DELETE FROM essia_arbo_childs_tecdoc")  
                    for data in jData:  
                        text_id = data['texId']
                        str_text = data['STR_DES_TEXT']
                        parent= data ['strIdParent']
                        str_id = str (data ['strId'])
                        descendants = data ['DESCENDANTS']
                        print(data)
                        if descendants == '0':
                            arbo_id = self.env['essia.arbo_childs_tecdoc'].search([('active', '=',True),('typ_motorisation_id','=',self.motorisation_id.typ_id),('str_id','=',self.arboresence_id.str_id)])
                            if not arbo_id:
                                        arbo_vals = {   'name':data['STR_DES_TEXT'],
                                                        'str_id':data['strId'],
                                                        'str_id_parent':data['strIdParent'],
                                                        'text_id':data['texId'],
                                                        'descendants':data['DESCENDANTS'],
                                                        'typ_motorisation_id':self.motorisation_id.typ_id,
                                                        'str_motorisation_id':self.arboresence_id.str_id}
                                        arbo_id = self.env['essia.arbo_childs_tecdoc'].create(arbo_vals)
                                        print ('-----------arbo_id_created:',arbo_id) 
                                        resarbo['domain']['arboresence_child_id'] = [('str_motorisation_id','=',self.arboresence_id.str_id),('typ_motorisation_id','=',self.motorisation_id.typ_id)]
                                        print (resarbo)
                        else: 
                            resarbo = {'domain': {'arboresence_child_id': []}}
                            path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/sectionbyparent/"
                            url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/"+str_id+"/6"
                            print('---URL : ',url1)
                            headers={'X-AUTH-TOKEN':token}
                            myResponse3 = requests.get(url1, headers = headers)
                            if(myResponse3.ok):
                                jData = json.loads(myResponse3.content.decode('utf-8'))
                                print ("The response contains {0} properties".format(len(jData)))
                                lengthJdata=int(format(len(jData)))
                                print("-----longueur JData-----",lengthJdata)
#                                 self._cr.execute("DELETE FROM essia_arbo_childs_tecdoc")  
                                for data in jData:
                                    text_id = data['texId']
                                    str_text = data['STR_DES_TEXT']
                                    str_id = str (data ['strId'])
                                    descendants = data ['DESCENDANTS']
                                    print(data)
                                    if descendants == '0' :
                                        arbo1_id = self.env['essia.arbo_childs_tecdoc'].search([('active', '=',True),('typ_motorisation_id','=',self.motorisation_id.typ_id),('str_id','=',self.arboresence_id.str_id)])
                                        if not arbo1_id:                         
                                                arbo1_vals = { 'name':data['STR_DES_TEXT'],
                                                                'str_id':data['strId'],
                                                                'str_id_parent':data['strIdParent'],
                                                                'text_id':data['texId'],
                                                                'descendants':data['DESCENDANTS'],
                                                                'typ_motorisation_id':self.motorisation_id.typ_id,
                                                                'str_motorisation_id':self.arboresence_id.str_id   }                 
                                                arbo1_id = self.env['essia.arbo_childs_tecdoc'].create(arbo1_vals)
                                                print ('-----------arb_fils_id1_created:',arbo1_id) 
                                                resarbo['domain']['arboresence_child_id'] = [('str_motorisation_id','=',self.arboresence_id.str_id),('typ_motorisation_id','=',self.motorisation_id.typ_id)]  
                                    else:
                                        resarbo = {'domain': {'arboresence_child_id': []}}
                                        path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/sectionbyparent/"
                                        url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/"+str_id+"/6"
                                        print('---URL : ',url1)
                                        headers={'X-AUTH-TOKEN':token}
                                        myResponse3 = requests.get(url1, headers = headers)
                                        if(myResponse3.ok):
                                            jData = json.loads(myResponse3.content.decode('utf-8'))
                                            print ("The response contains {0} properties".format(len(jData)))
                                            lengthJdata=int(format(len(jData)))
                                            print("-----longueur JData-----",lengthJdata)
#                                             self._cr.execute("DELETE FROM essia_arbo_childs_tecdoc")  
                                            for data in jData:
                                                text_id = data['texId']
                                                str_text = data['STR_DES_TEXT']
                                                str_id = str (data ['strId'])
                                                descendants = data ['DESCENDANTS']
                                                print(data)
#                                                 if descendants == 0 :
                                                arbo2_id = self.env['essia.arbo_childs_tecdoc'].search([('active', '=',True),('typ_motorisation_id','=',self.motorisation_id.typ_id),('str_id','=',self.arboresence_id.str_id)])
                                                if not arbo2_id:                          
                                                    arbo2_vals = {  'name':data['STR_DES_TEXT'],
                                                                    'str_id':data['strId'],
                                                                    'str_id_parent':data['strIdParent'],
                                                                    'descendants':data['DESCENDANTS'],
                                                                    'text_id':data['texId'],
                                                                    'typ_motorisation_id':self.motorisation_id.typ_id,
                                                                    'str_motorisation_id':self.arboresence_id.str_id}
                                                    arbo2_id = self.env['essia.arbo_childs_tecdoc'].create(arbo2_vals)
                                                    print ('-----------arb_fils_id2_created:',arbo2_id) 
                                                    resarbo['domain']['arboresence_child_id'] = [('str_motorisation_id','=',self.arboresence_id.str_id),('typ_motorisation_id','=',self.motorisation_id.typ_id)]  
                                                                                                     
        return resarbo 
    
    @api.multi
    @api.onchange('arboresence_child_id')
    def onchange_arboresence_child_id(self):
        print("----------yes---------") 
        res = {'domain': {'article_id': []}}
        if  self.arboresence_child_id :
                print('----motorisation_id : ',self.arboresence_id.typ_motorisation_id,"----ID Parent :",self.arboresence_id.str_id)
                tecdoc_obj = self.env['connexion.server']
                tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
                url = tecdoc_id.url
                token = tecdoc_id.token
                path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/partsBySection/"
                url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/"+self.arboresence_child_id.str_id+"/"+self.arboresence_child_id.text_id+"/6"
                print('---URL : ',url1)
                headers={'X-AUTH-TOKEN':token}
                myResponse3 = requests.get(url1, headers = headers)
                if(myResponse3.ok):
                    jData = json.loads(myResponse3.content.decode('utf-8'))
                    print ("The response contains {0} properties".format(len(jData)))
                    lengthJdata=int(format(len(jData)))
                    print("-----longueur JData-----",lengthJdata)
                    self._cr.execute("DELETE FROM essia_articlestecdoc")  
                    for data in jData:
                        supplier=data['supBrand']
                        designation=data['ART_COMPLETE_DES_TEXT']
                        ref_tecdoc_article = data['artArticleNr'] 
                        article_id = data ['artId']
                        print('article_id', article_id)
                        print(data)
                        product_search = self.env['product.product'].search([('default_code', '=',ref_tecdoc_article),('designation','=',designation)])
                        print(product_search)
                        if not product_search:
                            product_exist = False
                        else:
                            product_exist = True
                            print(product_exist)       
                        articles_vals = {   'name':designation,
                                            'designation':designation,
                                            'desig_tecdoc':self.arboresence_child_id.name,
                                            'supplier':supplier,
                                            'parent_stock':self.arboresence_id.name,
                                            'type_mot_id':self.motorisation_id.typ_id,
                                            'ref_tecdoc_article':ref_tecdoc_article,
                                            'article_id':article_id,
                                            'product_exist':product_exist}  
                        self.env['essia.articlestecdoc'].create(articles_vals)
                        print('art_id',self.article_id.article_id) 
                else:      
                    print("-------liste des articles est vide----------")
    
    @api.multi
    @api.onchange('arbo_stock_id')
    def onchange_arbo_stock_id(self):
        print("----------yes---------") 
        res = {'domain': {'articles_arbo_stock_id': []}}
        if  self.arbo_stock_id :
                print('----motorisation_id : ',self.arbo_stock_id.motorisation_id,"----ID Parent :",self.arbo_stock_id.str_id)
                tecdoc_obj = self.env['connexion.server']
                tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
                url = tecdoc_id.url
                token = tecdoc_id.token
                path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/sectionbyparent/"
                url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/"+self.arbo_stock_id.str_id+"/6"
                print('---URL : ',url1)
                headers={'X-AUTH-TOKEN':token}
                myResponse3 = requests.get(url1, headers = headers)
                if(myResponse3.ok):
                    jData = json.loads(myResponse3.content.decode('utf-8'))
                    print ("The response contains {0} properties".format(len(jData)))
                    lengthJdata=int(format(len(jData)))
                    print("-----longueur JData-----",lengthJdata)
                    self._cr.execute("DELETE FROM essia_articles_arbo_stock")  
                    for data in jData: 
                        text_id = data['texId']
                        str_text = data['STR_DES_TEXT']
                        parent= data ['strIdParent']
                        str_id = str (data ['strId'])
                        descendants = data ['DESCENDANTS']
                        print(data)
                        if descendants == '0':
                            product_stock_id = self.env['product.product'].search([('type_mot_id', '=', self.motorisation_id.typ_id),('parent_stock','=',self.arbo_stock_id.name),('desig_tecdoc','=',str_text)])
                            print(product_stock_id)
                            print('-----Parent :',self.arbo_stock_id.name)
                            print('parent :',str_text)
                            print('default_code',self.article_id.ref_tecdoc_article)
                            if product_stock_id :     
                                articles_stock_vals = { 'name':data['STR_DES_TEXT'],
                                                        'str_id':data['strId'],
                                                        'str_id_parent':data['strIdParent'],
                                                        'descendants':data['DESCENDANTS'],
                                                        'text_id':data['texId'],
                                                        'motorisation_id':self.motorisation_id.typ_id,
                                                        'parent':self.arbo_stock_id.name,
                                                        'client':self.client_id.id,}  
                                product_stock_id = self.env['essia.articles_arbo_stock'].create(articles_stock_vals)
                                print ('-----------article_stock_created:',product_stock_id) 
                                res['domain']['articles_arbo_stock_id'] =  [('motorisation_id', '=', self.motorisation_id.typ_id),('parent', '=',self.arbo_stock_id.name)]  
                                print('------res : ',res)   
                        else: 
                            path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/sectionbyparent/"
                            url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/"+str_id+"/6"
                            print('---URL : ',url1)
                            headers={'X-AUTH-TOKEN':token}
                            myResponse3 = requests.get(url1, headers = headers)
                            if(myResponse3.ok):
                                jData = json.loads(myResponse3.content.decode('utf-8'))
                                print ("The response contains {0} properties".format(len(jData)))
                                lengthJdata=int(format(len(jData)))
                                print("-----longueur JData-----",lengthJdata)
#                                 self._cr.execute("DELETE FROM essia_articles_arbo_stock")  
                                for data in jData:
                                    text_id = data['texId']
                                    str_text = data['STR_DES_TEXT']
                                    str_id = str (data ['strId'])
                                    descendants = data ['DESCENDANTS']
                                    parent= data ['strIdParent']
                                    print(data)
                                    if descendants == '0' : 
                                        product_stock_id1 = self.env['product.product'].search([('type_mot_id', '=', self.motorisation_id.typ_id),('parent_stock','=',self.arbo_stock_id.name),('desig_tecdoc','=',str_text)])
                                        print(product_stock_id1)
                                        print('-----Parent :',self.arbo_stock_id.name)
                                        print('parent :',str_text)
                                        print('default_code',self.article_id.ref_tecdoc_article)
                                        if product_stock_id1 :     
                                            articles_stock_vals1 = {    'name':data['STR_DES_TEXT'],
                                                                        'str_id':data['strId'],
                                                                        'str_id_parent':data['strIdParent'],   
                                                                        'descendants':data['DESCENDANTS'],
                                                                        'text_id':data['texId'],
                                                                        'motorisation_id':self.motorisation_id.typ_id,
                                                                        'parent':self.arbo_stock_id.name,
                                                                        'client':self.client_id.id,}  
                                            product_stock_id1 = self.env['essia.articles_arbo_stock'].create(articles_stock_vals1)
                                            print ('-----------article1_stock_created:',product_stock_id1) 
                                            res['domain']['articles_arbo_stock_id'] =  [('motorisation_id', '=', self.motorisation_id.typ_id),(('parent', '=',self.arbo_stock_id.name))]  
                                        print('------res : ',res)   
                                    else:                  
                                        path4 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/sectionbyparent/"
                                        url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/"+str_id+"/6"
                                        print('---URL : ',url1)
                                        headers={'X-AUTH-TOKEN':token}
                                        myResponse3 = requests.get(url1, headers = headers)
                                        if(myResponse3.ok):
                                            jData = json.loads(myResponse3.content.decode('utf-8'))
                                            print ("The response contains {0} properties".format(len(jData)))
                                            lengthJdata=int(format(len(jData)))
                                            print("-----longueur JData-----",lengthJdata) 
                                            for data in jData:
#                                                 self._cr.execute("DELETE FROM essia_articles_arbo_stock")  
                                                text_id = data['texId']
                                                str_text = data['STR_DES_TEXT']
                                                str_id = str (data ['strId'])
                                                descendants = data ['DESCENDANTS']
                                                parent= data ['strIdParent']
                                                print(data)
                                                product_stock_id2 = self.env['product.product'].search([('type_mot_id', '=', self.motorisation_id.typ_id),('parent_stock','=',self.arbo_stock_id.name),('desig_tecdoc','=',str_text)])
                                                print(product_stock_id2)
                                                print('-----Parent :',self.arbo_stock_id.name) 
                                                print('parent :',str_text) 
                                                print('default_code',self.article_id.ref_tecdoc_article)                          
                                                if product_stock_id2 :     
                                                    articles_stock_vals2 = {     'name':data['STR_DES_TEXT'],
                                                                                 'str_id':data['strId'],
                                                                                 'str_id_parent':data['strIdParent'],
                                                                                 'descendants':data['DESCENDANTS'],
                                                                                 'text_id':data['texId'],
                                                                                 'motorisation_id':self.motorisation_id.typ_id,
                                                                                 'parent':self.arbo_stock_id.name,
                                                                                 'client':self.client_id.id,}  
                                                    product_stock_id2 = self.env['essia.articles_arbo_stock'].create(articles_stock_vals2)
                                                    print ('-----------article2_stock_created:',product_stock_id2) 
                                                    res['domain']['articles_arbo_stock_id'] =  [('motorisation_id', '=', self.motorisation_id.typ_id),('parent', '=',self.arbo_stock_id.name)]  
                                                print('------res : ',res)       
                                                                                                     
        return res 
                
    @api.multi
    @api.onchange('articles_arbo_stock_id')
    def onchange_articles_arbo_stock_id(self):
        print("----------yes---------") 
        res = {'domain': {'article_id': []}}
        if  self.articles_arbo_stock_id :
                print('----motorisation_id : ',self.articles_arbo_stock_id.motorisation_id,"----ID Parent :",self.articles_arbo_stock_id.str_id)
                tecdoc_obj = self.env['connexion.server']
                tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
                url = tecdoc_id.url
                token = tecdoc_id.token
                path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/partsBySection/"
                url1 = "http://"+url+path3+self.motorisation_id.typ_id+"/"+self.articles_arbo_stock_id.str_id+"/"+self.articles_arbo_stock_id.text_id+"/6"
                print('---URL : ',url1)
                headers={'X-AUTH-TOKEN':token}
                myResponse3 = requests.get(url1, headers = headers)
                if(myResponse3.ok):
                    jData = json.loads(myResponse3.content.decode('utf-8'))
                    print ("The response contains {0} properties".format(len(jData)))
                    lengthJdata=int(format(len(jData)))
                    print("-----longueur JData-----",lengthJdata)
                    self._cr.execute("DELETE FROM essia_articlesstock")  
                    for data in jData:
                        supplier=data['supBrand']
                        designation=data['ART_COMPLETE_DES_TEXT']
                        ref_tecdoc_article = data['artArticleNr'] 
                        article_id = data ['artId'] 
                        print(data)
                        product_search = self.env['product.product'].search([('default_code', '=',ref_tecdoc_article),('designation','=',designation),('supplier','=',supplier)])
                        print(product_search)
                        if product_search:
                            articles_stock_vals = {
                                                'name':designation,
                                                'designation':designation,
                                                'desig_tecdoc':self.arboresence_child_id.name,
                                                'supplier':supplier,
                                                'parent_stock':self.arboresence_id.name,
                                                'type_mot_id':self.motorisation_id.typ_id,
                                                'ref_tecdoc_article':ref_tecdoc_article,
                                                'tecdoc_id':self.id,
                                                'client':self.client_id.id,
                                                'product_id':product_search.id,
                                            }  
                            article_stock_id = self.env['essia.articlesstock'].create(articles_stock_vals)
                            print ('-----------article_id_created:',article_stock_id)       
        else:      
            print("-------liste des articles stock est vide----------")
        return res        
                
                
                
                
                
                
                
                
                
                
                                
                
                
                
                
                
