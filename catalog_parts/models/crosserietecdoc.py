# -*- coding: utf-8 -*-
from odoo import models, fields , api

import requests

import json



class crosserie_tec_doc(models.Model):
    
    _name = 'essia.crosserietecdoc'

    name=fields.Char('description')
    crosserie_id = fields.Char('Body ID')
    marque_id = fields.Char('Brand ID')
    body = fields.Boolean('Body')
    active = fields.Boolean (default=True)
    modele = fields.Boolean(default=True)
      