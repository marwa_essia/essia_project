# -*- coding: utf-8 -*-
from odoo import api, fields, models, _ 
import requests
import json

class search(models.Model):
    _name = 'search.reference'
    
    _order = 'date desc, id desc'
    name = fields.Char('Name')
    ref_article = fields.Char('Reference')
    typ_ref = fields.Selection([('0', 'Product Reference'),('3', 'OE Reference')],'Reference Type', default='0')
    lang = fields.Char('Language')
    supplier = fields.Char('Supplier')
    designation = fields.Char('Designation')
    article_id = fields.Char('article id')
    display_number = fields.Char('Reference')
    brand = fields.Char('Brand')
    active = fields.Boolean(default=True)
    chaine = fields.Char(string="   ",readonly = True)
 
#     herit_id = fields.Many2one('sale.order', ondelete='cascade', string='responsible')
    articles_ids =  fields.One2many('article.stock','refer_id',string='Reference')
    base_id = fields.One2many('recherche.tecdoc','tec_id',string='Article')
    entrepot = fields.One2many('entrepot.ref','location_ids',string='Warehouse')
    client = fields.Many2one('res.partner',string = 'Customer')
    date = fields.Datetime(string='Date', required=True, readonly=True, index=True, copy=False, default=fields.Datetime.now)
    user = fields.Many2one('res.users','User', default=lambda self: self.env.user)
    product_id = fields.Many2one('product.product',string = 'Product')
    supp_filter = fields.Many2one('fabricant.filter', string='Supplier')
    
    @api.multi 
    def search_ref(self): 

        print('-----ref_type:',self.typ_ref)
        tecdoc_obj = self.env['connexion.server']
        tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
        url = tecdoc_id.url
        token = tecdoc_id.token
        self.name = self.ref_article
        path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlebyref/"
        url2 = "http://"+url+path+self.ref_article.replace(" ","").replace("/","").replace("-","").replace(".","").replace("!","").replace("$","").replace("(","").replace(")","").replace("'","").replace("&","").replace("*","").replace("_","").replace("#","").replace("[","").replace("]","").replace(":","").replace("%","").replace("+","").replace(",","").replace(";","").replace("<","").replace("=","").replace("?","").replace("@","").replace(">","").replace("^","").replace("{","").replace("|","").replace("}","").replace("~","").replace("s","").replace("`","").replace("\/","").replace("€","")+"/6/"+self.typ_ref
        headers={'X-AUTH-TOKEN':token}
        myResponse = requests.get(url2, headers = headers)
        if(myResponse.ok):
            jData = json.loads(myResponse.content.decode('utf-8'))
            old_search_ids = self.env['recherche.tecdoc'].search([('tec_id', '=',self.id)])
            for old_search in old_search_ids:
                old_search.unlink()
            old_search_product_ids = self.env['article.stock'].search([('refer_id', '=',self.id)])
            for old_search_product in old_search_product_ids:
                    old_search_product.unlink()
            old_search_entrep_ids = self.env['entrepot.ref'].search([('location_ids', '=',self.id)])
            for old_search_entrep in old_search_entrep_ids:
                    old_search_entrep.unlink()
            for data in jData:  
                supplier = data['SUPPLIER']
                supplier_id = data['SUPPLIER_ID']
                designation = data['DESIGNATION']
                article_id = data['ARTICLE_ID'] 
                reference = data['ARTTICLE_NUM'] 
                print(reference)
                print(data)
                product_id = self.env['product.product'].search([('default_code', '=',reference),('supplier', '=', supplier)])
                stock_id = self.env['stock.quant'].search([])
                print('---------product:',product_id)
                if not product_id:
                    product_exist = False
                else:
                    product_exist = True
                    search_product_id = self.env['article.stock'].search([('tec_article_id', '=', article_id),('refer_id', '=',self.id)])
                    if not search_product_id:
                        search_product_vals = {
                            'supplier':supplier,
                            'tec_article_id':article_id,
                            'refer_id':self.id,
                            'designation':designation,
                            'product_id':product_id.id,
                            'ref_article':product_id.default_code,
                            'prix_ht':product_id.lst_price,
                            'quantite':product_id.qty_available,
                            'client':self.client.id,}
                        self.env['article.stock'].create(search_product_vals)
                search_id = self.env['recherche.tecdoc'].search([('tec_article_id', '=', article_id),('tec_id', '=',self.id)])
                if not search_id:
                    search_vals={
                        'supplier':supplier,
                        'designation':designation,
                        'tec_article_id':article_id,
                        'tec_id':self.id,
                        'ref_article':data['ARTTICLE_NUM'],
                        'product_exist':product_exist,
                        'supplier_id':data['SUPPLIER_ID'],}
                    self.env['recherche.tecdoc'].create(search_vals)
                    supplier_filter_vals = { 'name':supplier,
                                             'supplier_id':data['SUPPLIER_ID'],
                                            'designation':data['DESIGNATION'],
                                        'reference' : data['ARTTICLE_NUM'] }
                    self.env['fabricant.filter'].create(supplier_filter_vals)


    
    @api.multi
    @api.onchange('supp_filter')
    def onchange_supp_filter(self):
        print ('---------yes------------')
#         res = {'domain': {'article_supp': []}}
         
        if(self.supp_filter):
#             self.base_id = [(5, 0, 0)]
            print("----Fabricant :", self.supp_filter.name)  
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id = tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlebyIdsupp/"
            url2 = "http://" + url + path + self.supp_filter.supplier_id + "/6/" + self.supp_filter.reference
            headers = {'X-AUTH-TOKEN':token}
            myResponseSupp = requests.get(url2, headers=headers)
            print("-----URL : ", url2)
            if(myResponseSupp.ok):
                jData = json.loads(myResponseSupp.content.decode('utf-8'))
                print ("The response contains {0} properties".format(len(jData)))
                old_search_ids = self.env['recherche.tecdoc'].search([('tec_id', '=', self.id)])
                for old_search in old_search_ids:
                    old_search.unlink()
                old_search_product_ids = self.env['article.stock'].search([('refer_id', '=', self.id)])
                for old_search_product in old_search_product_ids:
                    old_search_product.unlink()
                
                for data in jData: 
                    self.base_id = [(5, 0, 0)]
#                     self.article_supp.unlink()
#                     self._cr.execute("DELETE FROM essia_tecdoc_filter_supp ")  
                    supplier = data['SUPPLIER']
                    supplier_id = data['SUPPLIER_ID']
                    designation = data['ART_COMPLETE_DES_TEXT']
                    article_id = data['artId'] 
                    reference = data['artArticleNr'] 
                    print(reference)
                    print(data)
                    product_id = self.env['product.product'].search([('default_code', '=', reference), ('supplier', '=', supplier)])
#                     stock_id = self.env['stock.warehouse'].search([])
                   
                    if not product_id:
                            product_exist = False
                    else:
                            product_exist = True
                            search_product_id = self.env['article.stock'].search([('tec_article_id', '=', article_id), ('refer_id', '=', self.id)])
                            if not search_product_id:
                                search_product_vals = {
                                    'supplier':supplier,
                                    'tec_article_id':article_id,
                                    'refer_id':self.id,
                                    'designation':designation,
                                    'product_id':product_id.id,
                                    'ref_article':product_id.default_code,
                                    'prix_ht':product_id.lst_price,
                                    'quantite':product_id.qty_available,
                                     
                                    }
                                self.env['article.stock'].create(search_product_vals)
                    search_id = self.env['recherche.tecdoc'].search([('tec_article_id', '=', article_id), ('tec_id', '=', self.id)])
                    if not search_id:
                      
                        search_vals = {}
                        search_vals_lines = []
                        
                        search_vals.update({
                                'name': designation,
                                'supplier':supplier,
                                'designation':designation,
                                'tec_article_id':article_id,
                                'tec_id':self.id,
                                'ref_article':reference,
                                'product_exist':product_exist,
                                'supplier_id':supplier_id,
                               
                        })
                         
#                 search_vals.update(self.env['recherche.tecdoc']
                    
                        for data in jData: 
                           
                            ctx = self._context.copy()     
                            search_vals_lines.append((0, 0, search_vals)) 
                            ctx.update({'default_base_id':search_vals_lines})
                            view_id = self.env.ref('catalog_parts.view_catalogue_form').id
#                             search_vals.update(self.env['search.reference'])
                          
                            print ('--------context:', ctx)
                            self.base_id = [(0, 0, search_vals)]
                      
                        self.base_id = search_vals_lines
                        
                        
                num = 0
                
                while(num <100 ) :
                    
                 search_stock = self.env['stock.quant'].search([('product_id.supplier', '=', supplier),('product_id.default_code', '=',reference),('quantity','>=',0),('id', '=',num)]) 
                 num += 1   
                 if  search_stock :
                     entrep_vals={
                         
                       'article':search_stock.product_id.name,
                       'lieu':search_stock.location_id.name,
                       'location_ids':self.id,
                       'societe':search_stock.company_id.name,
                       'enstock':search_stock.quantity,
                       'reference':search_stock.product_id.default_code,
                       'fabricant':search_stock.product_id.supplier,
                      
                        }
                     self.env['entrepot.ref'].create(entrep_vals)
              
        print('----test:',self.read())     
        return True
    
        