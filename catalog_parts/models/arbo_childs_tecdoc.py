# -*- coding: utf-8 -*-
from odoo import models, fields , api

import requests

import json

class arbo_childs_tec_doc(models.Model):
    
    _name = 'essia.arbo_childs_tecdoc'
    name = fields.Char('Description')
    active = fields.Boolean(default= True)
    text_id = fields.Char('Text id')
    str_id = fields.Char('Str id')
    str_id_parent = fields.Char('Str_id_parent')
    typ_motorisation_id=fields.Char('Motorisation type ID')
    str_motorisation_id = fields.Char('Motorisation Str ID ')
    str_id_parent=fields.Char('Parent ID')
    descendants=fields.Char('Descending')
    str_descendant=fields.Integer('Descending')
    arbo1 = fields.Html('Tree essia')
    parent_id=fields.Many2many('essia.arboresencetecdoc','parent_id')
#     tec_id_arbo = fields.Many2one('essia.searchbyvehicule', ondelete='cascade', string='tecdoc_arbo_Id')
#     childs_ids=fields.One2many('essia.searchbyvehicule','parent_id', string='tree')
#     
    