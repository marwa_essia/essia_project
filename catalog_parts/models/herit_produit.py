# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 


class herit_produit(models.Model):
    _inherit = 'product.product'
    supplier = fields.Char('Supplier')
    designation = fields.Char('Designation')
    article_id =  fields.Char(string='Article id',invisible ="1")
    supplier_id = fields.Char('Supplier id')
    desig_distributeur = fields.Char('Distributor designation')
    marque = fields.Char('Brand')
    parent_stock = fields.Char('parent')
    type_mot_id = fields.Char ('type_mot_id')
    str_id= fields.Char('str_id')
    desig_tecdoc= fields.Char('desig_tecdoc')
    str_id_stock = fields.Char('str_id_stock')
    
   