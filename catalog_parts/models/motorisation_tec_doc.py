# -*- coding: utf-8 -*-
from odoo import api, fields, models, _ 
import requests
import json

class motorisation_tec_doc(models.Model):
    _name = 'essia.motorisation_tec_doc'
  
    
    name = fields.Char('Model')
    typ_id = fields.Char('Typ id')
    active = fields.Boolean(default= True)
    test= fields.Boolean(default= True)  
    model_tec_id=fields.Char('Model Tec DOc') 
    str_id=fields.Char('STR ID')     
 
    