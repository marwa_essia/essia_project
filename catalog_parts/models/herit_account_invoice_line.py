# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 

class SaleOrderLine(models.Model):
    _inherit = 'account.invoice.line'
    
    
    fabricant = fields.Char('Supplier',compute = '_compute_fab')
    product = fields.Many2one('product.product',string = 'Product')
    @api.multi
    @api.onchange('product_id')
    def product_id_fabricant(self):
        
        if self.product_id:
            self.fabricant = self.product_id.supplier
    
    @api.one
    @api.depends('product_id')
    def _compute_fab(self) :
      
        self.fabricant = self.product_id.supplier
        
        