# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class categorie(models.Model):
    _name = 'marque.categorie'
    name = fields.Char('Category')
    supplier_id = fields.Char('Supplier id')
    supplier = fields.Char('Supplier ')
    ref_article = fields.Char('Ref article')
    marque = fields.Char('Brand')
    designation = fields.Char('Designation')

