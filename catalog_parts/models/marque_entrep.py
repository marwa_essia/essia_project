# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class marque_veh(models.Model):
    _name = 'marque.entrep'
    name = fields.Char('Category')
    supplier = fields.Char('Supplier ')
    supplier_id = fields.Char('supplier id')
    ref_article = fields.Char('Ref article')
    marque = fields.Char('Brand')
    designation = fields.Char('Designation')