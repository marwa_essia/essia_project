# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json

class search_fab(models.Model):
    _name = 'search.fabricant'
    _order = 'date desc, id desc'
    fabricant = fields.Many2one('fabricant.tecdoc',string='Supplier')
    categorie = fields.Many2one('categorie.fabricant',string='Designation')
    categorie_stock = fields.Many2one('categorie.stock',string='category')
    marque = fields.Many2one('marque.fabricant',string='Brand')
    marque_stock = fields.Many2one('marque.stock',string='Brand')
    mar_cat = fields.Many2one('marque.categorie',string='category')
    fabricant_ids =  fields.One2many('fabricant.stock','fa_id',string='supplier')
    tecdoc_fab =  fields.One2many('product.tecdoc','tab_id',string='product tecdoc')
    marque_fab =  fields.One2many('marq.tecdoc','marq_id',string='Brand')
    product_exist = fields.Boolean('Product Exist')
    marque_duplication = fields.Many2one('marque.dupl',string='Brand')
    categorie_duplication = fields.Many2one('categorie.dupl',string='Designation')
    categorie_stock_article = fields.Many2one('artcateg.stock',string='category')
    categorie_marque_stock = fields.Many2one('categorie.marque',string='category')
    client = fields.Many2one('res.partner',string = 'Customer')
    date = fields.Datetime(string='Date', required=True, readonly=True, index=True, copy=False, default=fields.Datetime.now)
    name = fields.Char('Name')
    desig = fields.Char('Designation')
    user = fields.Many2one('res.users','User', default=lambda self: self.env.user)
    categ_tec = fields.Many2one('categorie.fabricant',string='Designation')
    

    @api.multi
    @api.onchange('fabricant')
    def onchange_fabricant(self): 
        res = {'domain': {'categorie': []}}
        res2 = {'domain': {'marque': []}}
        res3 = {'domain': {'categorie_stock': []}}
        res4 = {'domain': {'marque_stock': []}}
        if (self.fabricant):
            
     # show the differents categories created by supplier in search tecdoc
            self.name = self.fabricant.name
            print("fab ID ,Nom fab: ",(self.fabricant , self.fabricant.name, self.fabricant.supplier_id))
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path1 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/GetDesignationarticlesBySupp/"
            url1 = "http://"+url+path1+self.fabricant.supplier_id+"/6"
            headers={'X-AUTH-TOKEN':token}
            myResponse1 = requests.get(url1, headers = headers)
            if(myResponse1.ok):
                jData = json.loads(myResponse1.content.decode('utf-8'))
                self._cr.execute("DELETE FROM categorie_fabricant")
                print ("The response contains {0} properties".format(len(jData)))
                for dat in jData:
                    article = dat['ART_COMPLETE_DES_TEXT']    
                    print(article) 
                    categ_tecdoc__id = self.env['categorie.fabricant'].search([('article', '=', article),('supplier', '=', self.fabricant.name),('test','=',True)])
                    print ('-------------cross_id_exist:',categ_tecdoc__id)  
                    if not categ_tecdoc__id:
                        catg_vals = {'name':dat['ART_COMPLETE_DES_TEXT'],'supplier':self.fabricant.name,}
                        categ_tecdoc__id = self.env['categorie.fabricant'].create(catg_vals)  
                        print ('-------------categ_tecdoc__id_created:',categ_tecdoc__id)
                        res['domain']['categorie'] =  [('supplier', '=', self.fabricant.name)]
                # show the differents brands created by supplier in search tecdoc   
                path2 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/GetMnfBySupp/"
                url2 = "http://"+url+path2+self.fabricant.supplier_id+"/6"
                myResponse2 = requests.get(url2, headers = headers)
                if(myResponse2.ok):
                    items = json.loads(myResponse2.content.decode('utf-8'))
                    self._cr.execute("DELETE FROM marque_fabricant WHERE supplier =%s", (self.fabricant.name,))
                    for item in items:
                        marque = item['MANUFACTURER']
                        marq_id = self.env['marque.fabricant'].search([('marque', '=', marque),('supplier', '=', self.fabricant.name),('test','=',True)])
                        if not marq_id:
                            marque_vals = {
                                  'name':item['MANUFACTURER'],
                                  'supplier':self.fabricant.name,
                             
                            }
                            marq_id = self.env['marque.fabricant'].create(marque_vals)  
                            print ('-------------marq_id_created:',marq_id)
                        res['domain']['marque'] =  [('supplier', '=', self.fabricant.name)]
                #show list of brands in stock  
                path3 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlesandmnfbysupplier/"
                url3 = "http://"+url+path3+self.fabricant.supplier_id+"/6"
                myResponse3 = requests.get(url3, headers = headers)
                if(myResponse3.ok):
                    jData = json.loads(myResponse3.content.decode('utf-8'))
                    self._cr.execute("DELETE FROM marque_stock")
                    self._cr.execute("DELETE FROM marque_entrep")
                    self._cr.execute("DELETE FROM veh_aff")
                    for data in jData:
                        marque = data['MANUFACTURER']
                        ref_article = data['artArticleNr']
                        article_id = str(data['artId'])                     
                        product_search = self.env['product.product'].search([('supplier', '=', self.fabricant.name),('default_code', '=', ref_article)])
                        if product_search:
                          #show list of affected vehicules
                            path4 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/vhaffected/manuf/"
                            url4= "http://"+url+path4+article_id+"/1"
                            print(article_id)
                            myResponse4 = requests.get(url4, headers = headers)
                            if(myResponse4.ok): 
                                jDat = json.loads(myResponse4.content.decode('utf-8'))
                                for dat in jDat:
                                    product_marque = { 'name':dat['mfaBrand'],'supplier':self.fabricant.name,}
                                    self.env['veh.aff'].create(product_marque)
                #delete duplication du marque
                if(myResponse2.ok):
                    items = json.loads(myResponse2.content.decode('utf-8'))
                    self._cr.execute("DELETE FROM marque_dupl")
                    for item in items:
                     
                            marque = item['MANUFACTURER']
                            marq_id = self.env['veh.aff'].search([('name', '=', marque),('supplier', '=', self.fabricant.name)])  
                            if marq_id:
                                marque_stock_vals = {'name':item['MANUFACTURER'],'supplier':self.fabricant.name, }
                                self.env['marque.dupl'].create(marque_stock_vals)    
                            res['domain']['marque_duplication'] =  [('supplier', '=', self.fabricant.name)]
                #show list of caegories in stock
                path5 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlesbysupplier/"
                url5 = "http://"+url+path3+self.fabricant.supplier_id+"/6"
                myResponse5 = requests.get(url5, headers = headers)
                if(myResponse5.ok):
                    jData = json.loads(myResponse5.content.decode('utf-8'))
                    self._cr.execute("DELETE FROM categorie_stock")
                    for data in jData:
                        article = data['ART_COMPLETE_DES_TEXT']
                        art_id = data['artId']
                        ref_article = data['artArticleNr']
                        categ_stock_id = self.env['product.product'].search([('supplier', '=', self.fabricant.name),('default_code', '=', ref_article)])
                        if categ_stock_id : 
                            categ_stock_vals = { 'name':article,'supplier':self.fabricant.name, }
                            categ_stock_id = self.env['categorie.stock'].create(categ_stock_vals) 
            #delete duplication of categories in stock
                if(myResponse1.ok):
                    jData = json.loads(myResponse1.content.decode('utf-8'))
                    self._cr.execute("DELETE FROM categorie_dupl")
                    for dat in jData:     
                        article = dat['ART_COMPLETE_DES_TEXT']  
                        categ_id = self.env['categorie.stock'].search([('name', '=', article),('supplier', '=', self.fabricant.name)])  
                        if categ_id:
                            categorie_duplication_id = { 'name':article, 'supplier':self.fabricant.name,}
                            self.env['categorie.dupl'].create(categorie_duplication_id)    
                        res['domain']['categorie_duplication'] =  [('supplier', '=', self.fabricant.name)]  
        return res 
            
    @api.multi 
    @api.onchange('categorie') 
    def categorie_onchange_tecdoc(self):
      if (self.categorie):
       print("cat ID ,Nom cat: ",(self.categorie , self.categorie.name))
       tecdoc_obj = self.env['connexion.server']
       tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
       url = tecdoc_id.url
       token = tecdoc_id.token
       path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlesbysupplier/"
       url ="http://"+ url+path+self.fabricant.supplier_id+"/6"
       headers={'X-AUTH-TOKEN':token}
       myResponse = requests.get(url, headers = headers)
       if(myResponse.ok):
        jData = json.loads(myResponse.content.decode('utf-8'))
        self._cr.execute("DELETE FROM categ_art ")
        for data in jData:
            article = data['ART_COMPLETE_DES_TEXT']
            art_id = data['artId']
            ref_article = data['artArticleNr']
            product_search = self.env['product.product'].search([('default_code', '=',ref_article),('supplier', '=', self.fabricant.name)])
            if not product_search:
                product_exist = False
            else:
                product_exist = True
            if self.categorie.name == article :  
                  search_vals= {
                     'supplier':self.fabricant.name,
                     'supplier_id':self.fabricant.supplier_id,
                     'designation':self.categorie.name,
                     'ref_article':ref_article,
                     'product_exist':product_exist , 
                     'article_id':art_id, 
                      }
                  print(ref_article)
                  self.env['categ.art'].create(search_vals) 
        return {}
                      
      
    @api.multi 
    @api.onchange('marque')
    def marque_onchange_tecdoc(self):
      res = {'domain': {'mar_cat': []}}
      if (self.marque):
       print("cat ID ,Nom cat: ",(self.marque , self.marque.name))
       tecdoc_obj = self.env['connexion.server']
       tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
       url = tecdoc_id.url
       token = tecdoc_id.token
       path4 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/GetDesigarticlesAndMnfBySupp/"
       url4 ="http://"+ url+path4+self.fabricant.supplier_id+"/6"
       headers={'X-AUTH-TOKEN':token}
       myResponse4 = requests.get(url4, headers = headers)
       if(myResponse4.ok):
        jData = json.loads(myResponse4.content.decode('utf-8'))
        self._cr.execute("DELETE FROM marque_categorie WHERE marque =%s", (self.marque.name,))
        self._cr.execute("DELETE FROM marque_categorie WHERE supplier =%s", (self.fabricant.name,))
        for data in jData:
            article = data['ART_COMPLETE_DES_TEXT']
            marque = data['MANUFACTURER']
            supplier_id = data['SUPPLIER_ID']
            if marque == self.marque.name:
                art_vals = {
                                     'name':article,
                                     'supplier':self.fabricant.name,
                                     'marque':data['MANUFACTURER'],
                                         }
                self.env['marque.categorie'].create(art_vals)
            res['domain']['mar_cat'] =  [('supplier', '=', self.fabricant.name),('marque', '=', self.marque.name)]   
        return res 
    
    @api.multi 
    @api.onchange('mar_cat')
    def marque_categorie_tecdoc(self): 
        if self.mar_cat: 
            print("designation ,reference: ",(self.mar_cat.name )) 
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlesandmnfbysupplier/"
            url ="http://"+ url+path+self.fabricant.supplier_id+"/6"
            headers={'X-AUTH-TOKEN':token}
            myResponse = requests.get(url, headers = headers)
            if(myResponse.ok):
                jData = json.loads(myResponse.content.decode('utf-8'))
                self._cr.execute("DELETE FROM many_art ")
                for data in jData:
                    article = data['ART_COMPLETE_DES_TEXT']
                    marque = data['MANUFACTURER']
                    ref_article = data['artArticleNr']
                    supplier_id = data['SUPPLIER_ID']
                    article_id = data['artId']
                    product_search = self.env['product.product'].search([('default_code', '=',ref_article),('supplier', '=', self.fabricant.name)])
                    if not product_search:
                        product_exist = False
                    else:
                        product_exist = True
                    if self.mar_cat.name == article and self.mar_cat.marque == marque:
                            articles_tecdoc_vals={
                                'supplier':self.fabricant.name,
                                'designation':article,
                                'ref_article':ref_article,
                                'supplier_id':data['SUPPLIER_ID'],
                                'marque':marque,
                                'product_exist':product_exist,
                                'article_id':article_id, }
                            self.env['many.art'].create(articles_tecdoc_vals) 
       
    @api.multi 
    @api.onchange('marque_duplication')
    def marque_stock_change(self):
        res = {'domain': {'categorie_marque_stock': []}}
        if (self.marque_duplication):
            print("Nom marque: ",(self.marque_duplication.name ))
            mar_stock = self.env['product.product'].search([('marque', '=', self.marque_stock.name)])
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path4 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlesandmnfbysupplier/"
            url4 ="http://"+ url+path4+self.fabricant.supplier_id+"/6"
            headers={'X-AUTH-TOKEN':token}
            myResponse4 = requests.get(url4, headers = headers)
            if(myResponse4.ok):
                jData = json.loads(myResponse4.content.decode('utf-8'))
                self._cr.execute("DELETE FROM artcateg_stock")
                for data in jData:
                    ref_article = data['artArticleNr']
                    article = data['ART_COMPLETE_DES_TEXT']
                    marque = data['MANUFACTURER']
                    article_id = data['artId']
                    mar_stock = self.env['product.product'].search([('supplier','=', self.fabricant.name),('default_code', '=',ref_article)])
                    if  mar_stock and marque == self.marque_duplication.name:
                        art_vals = { 'name':article,
                                     'supplier':self.fabricant.name,
                                     'ref_article':mar_stock.default_code, }
                        self.env['artcateg.stock'].create(art_vals) 
            res['domain']['categorie_stock_article'] =  [('supplier', '=', self.fabricant.name)] 
            path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/GetDesignationarticlesBySupp/"
            url1 = "http://"+url+path+self.fabricant.supplier_id+"/6"
            headers={'X-AUTH-TOKEN':token}
            myResponse = requests.get(url1, headers = headers)
            if(myResponse.ok):
                jData = json.loads(myResponse.content.decode('utf-8'))
                self._cr.execute("DELETE FROM categorie_marque")
                for dat in jData:     
                    article = dat['ART_COMPLETE_DES_TEXT']    
                    categ_id = self.env['artcateg.stock'].search([('name', '=', article),('supplier', '=', self.fabricant.name)])  
                    if categ_id:
                        categorie_vals = { 'name':article,
                                           'supplier':self.fabricant.name,
                                           'marque':self.marque_duplication.name, }
                        self.env['categorie.marque'].create(categorie_vals)    
                    res['domain']['categorie_marque_stock'] =  [('supplier', '=', self.fabricant.name),('marque', '=', self.marque_duplication.name)] 
        return res
    @api.onchange('categorie_marque_stock')
    def article_marque_stock(self):
        if (self.categorie_marque_stock):
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id = tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path4 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlesandmnfbysupplier/"
            url4 = "http://" + url + path4 + self.fabricant.supplier_id + "/6"
            headers = {'X-AUTH-TOKEN':token}
            myResponse4 = requests.get(url4, headers=headers)
            if(myResponse4.ok):
                jData = json.loads(myResponse4.content.decode('utf-8'))
                self._cr.execute("DELETE FROM marque_stck")
                for data in jData:
                    article = data['ART_COMPLETE_DES_TEXT']
                    marque = data['MANUFACTURER']
                    ref_article = data['artArticleNr']
                    article_id  = data['artId']
                    mar_stock = self.env['product.product'].search([('supplier','=', self.fabricant.name),('name', '=',self.categorie_marque_stock.name),('default_code', '=',ref_article),('article_id', '=',article_id)])
                    if  mar_stock and marque == self.marque_duplication.name:
                        article_stock_vals = {  'name':article,
                                                'supplier_id':self.fabricant.supplier_id,
                                                'supplier':self.fabricant.name,
                                                'ref_article':mar_stock.default_code,
                                                'marque':marque,
                                                'designation':mar_stock.name,
                                                'qte':mar_stock.qty_available,
                                                'product_id':mar_stock.id,
                                                'client':self.client.id, }
                        self.env['marque.stck'].create(article_stock_vals)  
       
    @api.multi 
    @api.onchange('categorie_duplication','client') 
    def categorie_stock_onchange(self):
        if (self.categorie_duplication):
            tecdoc_obj = self.env['connexion.server']
            tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
            url = tecdoc_id.url
            token = tecdoc_id.token
            path4 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/articlesbysupplier/"
            url4 ="http://"+ url+path4+self.fabricant.supplier_id+"/6"
            headers={'X-AUTH-TOKEN':token}
            myResponse4 = requests.get(url4, headers = headers)
            self._cr.execute("DELETE FROM categorie_stck ")
            if(myResponse4.ok):
                jData = json.loads(myResponse4.content.decode('utf-8'))
                for data in jData:
                    ref_article = data['artArticleNr']  
                    article_id = data['artId']
                    print("cat ID ,Nom cat: ",(self.categorie_duplication , self.categorie_duplication.name))
                    tab_prod = self.env['product.product'].search([('name', '=', self.categorie_duplication.name),('supplier', '=', self.fabricant.name),('default_code', '=', ref_article),('article_id', '=', article_id)])
                    self.desig = self.categorie_duplication.name 
                    if tab_prod :
                          articles_stock_vals = {   'supplier':self.fabricant.name,
                                                    'supplier_id':self.fabricant.supplier_id,
                                                    'designation':tab_prod.name,
                                                    'ref_article':tab_prod.default_code,
                                                    'qte':tab_prod.qty_available,
                                                    'product_id':tab_prod.id, 
                                                    'client':self.client.id, }
                          self.env['categorie.stck'].create(articles_stock_vals)
                        
      #***** Remplir le champ fabricant ******#  
      
    def search_fabricant(self):
        tecdoc_obj = self.env['connexion.server']
        tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
        url = tecdoc_id.url
        token = tecdoc_id.token
        path4 = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/allsuppliers"
        url4 ="http://"+ url+path4
        headers={'X-AUTH-TOKEN':token}
        myResponse4 = requests.get(url4, headers = headers)
        if(myResponse4.ok):
            jData = json.loads(myResponse4.content.decode('utf-8'))
            for data in jData:
                supplier = data['SUPPLIER']  
                supplier_id = data['SUPPLIER_ID'] 
                tab_prod = self.env['fabricant.tecdoc'].search([('name', '=', supplier),('supplier_id', '=', supplier_id)])
                
                if not tab_prod:
                    supplier_vals = {  'name':supplier,
                                       'supplier_id': supplier_id, }
                    self.env['fabricant.tecdoc'].create(supplier_vals)
            return True
       # ***********************************************#
        
        
 
  
