# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 


class respartner(models.Model):
    _inherit = 'res.partner'
    _order = "montant desc,id"

    montant = fields.Monetary(compute='_compute_sale_montant_count',string ='Amount due')
    sale_avoir_count = fields.Integer(compute='_compute_sale_avoir_count', string='Credit Notes')

    def _compute_sale_avoir_count(self):
        avoir_data = self.env['account.invoice'].read_group(domain=[('partner_id', 'child_of', self.ids),('type','=','out_refund')],
                                                      fields=['partner_id'], groupby=['partner_id'])
        # read to keep the child/parent relation while aggregating the read_group result in the loop
        partner_child_ids = self.read(['child_ids'])
        mapped_data = dict([(m['partner_id'][0], m['partner_id_count']) for m in avoir_data])
        for partner in self:
            # let's obtain the partner id and all its child ids from the read up there
            item = next(p for p in partner_child_ids if p['id'] == partner.id)
            partner_ids = [partner.id] + item.get('child_ids')
            # then we can sum for all the partner's child
            partner.sale_avoir_count = sum(mapped_data.get(child, 0) for child in partner_ids)
    
    def _compute_sale_montant_count(self):
    
        account_invoice_report = self.env['account.invoice']
        if not self.ids:
            self.montant = 0.0
            return True

        user_currency_id = self.env.user.company_id.currency_id.id
        all_partners_and_children = {}
        all_partner_ids = []
        for partner in self:
            # price_total is in the company currency
            all_partners_and_children[partner] = self.with_context(active_test=False).search([('id', 'child_of', partner.id)]).ids
            all_partner_ids += all_partners_and_children[partner]
            where_query = account_invoice_report._where_calc([
            ('partner_id', 'in', all_partner_ids), ('state', 'not in', ['draft', 'cancel']),
            ('type', 'in', ('out_invoice', 'out_refund'))
              ])
            account_invoice_report._apply_ir_rules(where_query, 'read')
            from_clause, where_clause, where_clause_params = where_query.get_sql()
            query = """
                    SELECT SUM(residual) as total, partner_id
                    FROM account_invoice account_invoice
                   WHERE %s
                   GROUP BY partner_id
                """ % where_clause
            self.env.cr.execute(query, where_clause_params)
            price_totals = self.env.cr.dictfetchall()
            for partner, child_ids in all_partners_and_children.items():
                partner.montant = sum(price['total'] for price in price_totals if price['partner_id'] in child_ids)
    #@api.multi
    #def action_view_partner_avoir(self):
    #    self.ensure_one()
    #   action = self.env.ref('account.action_invoice_refund_out_tree').read()[0]
    #   action['domain'] = literal_eval(action['domain'])
    #   action['domain'].append(('partner_id', 'child_of', self.id))
    #   return action