# -*- coding: utf-8 -*-
from odoo import api, fields, models, _ 
import requests
import json

class fabricants_filter(models.Model):
    _name = 'fabricant.filter'
    name = fields.Char('Name')
    supplier_id = fields.Char('Supplier ID')
    reference = fields.Char('refrence')
    