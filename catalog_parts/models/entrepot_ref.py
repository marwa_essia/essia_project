# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json

class enterepot_ref(models.Model):
    _name = 'entrepot.ref'
    article = fields.Char('Product')
    reference = fields.Char('Reference')
    fabricant = fields.Char('Supplier')
    lieu = fields.Char('Location')
    societe = fields.Char('Company')
    enstock = fields.Char('On Hand')
    location_ids = fields.Many2one('search.reference', ondelete='cascade', string='tecdoc')