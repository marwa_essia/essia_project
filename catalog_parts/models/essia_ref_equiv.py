# -*- coding: utf-8 -*-
from odoo import api, fields, models, _ 
import requests
import json

class reference_equiv(models.Model):
    _name ='essia.ref_equiv'
    
    name= fields.Char('Name')
    supplier = fields.Char('Fabricant')
    supplier_id = fields.Char('supplier id')
    ref_article = fields.Char('Référence')
    designation = fields.Char('Désignation')
    quantite = fields.Char('Qte')
    prix_ht = fields.Char('Prix HT')
    prix_ttc = fields.Char('Prix TTC')
    entrepot = fields.Char('Entrepôt')
    refer_id = fields.Many2one('search.reference', ondelete='cascade', string='article')
#     liste_id = fields.Many2one('search.fabricant', ondelete='cascade', string='article')
   
    tec_article_id = fields.Char('TEECDOC Article Id')
    product_id = fields.Many2one('product.product', string='Product')
    #st_id = fields.Many2one('sale.order', string='Order Id')
#     equiv_ids =  fields.One2many('article.stock','equiv_id',string='reference')
        
    def devis(self):
        products = {}
        product_line = []
        ctx = self._context.copy()
        print ('--------context:',ctx)
        view_id = self.env.ref('sale.view_order_form').id
        products.update({'product_id': self.product_id.id,'name':self.designation,'designation':self.designation,'reference':self.ref_article,'fabricant':self.supplier, 'product_uom_qty':1, 'product_uom':self.product_id.uom_id.id,'list_price':self.prix_ht})
        product_line.append((0, 0, products))

        ctx.update({'default_order_line':product_line})
        print ('--------context:',ctx)
#         order_line_id = self.env['sale.order.line'].create(order_line_vals)
#         print('-----product_id:',self.product_id.id)
#         print('-----order_line_id:',order_line_id)
#         context['order_line'] = self.order_line_id.id
        return {

        'name': 'devis stock',
    
        'type': 'ir.actions.act_window',
    
        'res_model': 'sale.order',
    
        'view_mode': 'form',
    
        'view_type': 'form',
    
        'target': 'new',
#         'view_id': view_id,
#         'res_id': self.id,
        'context':ctx,

      } 
#     @api.multi
#     def equiv_ref(self): 
#       
#         view_id = self.env.ref('catalog_parts.equiv_stock_form_view').id
#         tecdoc_obj = self.env['connexion.server']
#         tecdoc_id =  tecdoc_obj.search([('active', '=', True)])
#         url = tecdoc_id.url
#         token = tecdoc_id.token
#         path = "/espace-travail/espace-web-service/web_service_tecdoc2/web/app_dev.php/equivalentsref/"
#         url2 = "http://"+url+path+self.tec_article_id+"/4"+"/6"
#         print("-----URL" , url2)
#         headers={'X-AUTH-TOKEN':token}
#         myResponse = requests.get(url2, headers = headers)
#                      
#         if(myResponse.ok):
#             jData = json.loads(myResponse.content.decode('utf-8'))
# #             old_search_ids = self.env['recherche.tecdoc'].search([('tec_id', '=',self.id)])
# #             for old_search in old_search_ids:
# #                 old_search.unlink()
# #             old_search_product_ids = self.env['essia_ref_equiv'].search([('refer_id', '=',self.id)])
# #             for old_search_product in old_search_product_ids:
# #                 old_search_product.unlink()
#                         
#                          
#             for data in jData:  
#                 supplier = data['SUPPLIER']
#                 supplier_id = data['SUPPLIER_ID']
#                 designation = data['DESIGNATION']
#                 article_id = data['ARTICLE_ID'] 
#                 reference = data['CLEAR_NUM'] 
#                 print(reference)
#                 print(data)
#                 product_id = self.env['product.product'].search([('default_code', '=',reference),('supplier', '=', supplier)])
#              
#                 if not product_id:
#                     product_exist = False
#                 else:
#                     product_exist = True
#                 search_product_id = self.env['essia.reference_equiv'].search([('tec_article_id', '=', article_id)])
#                 if search_product_id:
#                     search_product_vals = {
#                                                  'supplier':supplier,
#                                                  'tec_article_id':article_id,
#                                                  'refer_id':self.id,
#                                                  'designation':designation,
#     #                                              'product_id':product_id.id,
#                                                  'ref_article':reference,
#                                                  'prix_ht':product_id.lst_price,
#                                                  'quantite':product_id.qty_available,
#                                               
#                                                  
#                                                 
#                                                  }
#                     equiv_id=self.env['essia.ref_equiv'].create(search_product_vals)
#                     print ('-------------equiv_id_created:',equiv_id)   
#                                                
#              
#                 return {
#      
#                             'name': 'Références Equivalentes',
#                          
#                             'type': 'ir.actions.act_window',
#                          
#                             'res_model': 'essia.wizard_ref_equiv_stock',
#                          
#                             'view_mode': 'form',
#                          
#                             'view_type': 'form',
#                          
#                             'target': 'new',
#                             
#                             'view_id': view_id,
#                     #         'res_id': self.id,
# #                             'context':ctx,
#  
#                         } 
#                            