# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json
import logging

class connexion(models.Model):
    _name = 'connexion.server'
    url = fields.Char('url')
    token = fields.Char('Token')
    active = fields.Boolean(default=True)
    name = fields.Char('description')  
