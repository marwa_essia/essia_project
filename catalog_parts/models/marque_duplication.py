# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class marquestock_test(models.Model):
    _name = 'marque.dupl'
    name = fields.Char('Category')
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('Supplier id')
    ref_article = fields.Char('Ref article')
    marque = fields.Char('Brand')
    designation = fields.Char('Designation')