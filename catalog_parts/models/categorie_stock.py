# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json


class categorie(models.Model):
    _name = 'categorie.stock'
    name = fields.Char('Catgegory')
    art_id = fields.Char('Art id')
    supplier = fields.Char('Supplier ')
    supplier_id = fields.Char('Supplier id')
    test = fields.Boolean(default=True)
