# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 
import requests
import json

class tecdoc(models.Model):
    _name = 'recherche.tecdoc'
    
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('Supplier id')
    ref_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    tec_id = fields.Many2one('search.reference', ondelete='cascade', string='Tecdoc')
    tec_article_id = fields.Char('TEECDOC Article Id')
    product_exist = fields.Boolean('Product Exist')
    marque = fields.Char('Brand')


    
    @api.multi
    def stock(self):
        ctx = self._context.copy()
        active_id = self.id
        print ('----active_id:',active_id)
   
        ctx.update({'default_default_code': self.ref_article,'default_type':'product','default_supplier': self.supplier,'default_designation': self.designation,'default_article_id': self.tec_article_id,'default_supplier_id': self.supplier_id,'default_name': self.designation,'default_marque': self.marque})
        view_id = self.env.ref('product.product_normal_form_view').id
        model = 'product.product'
        return {
         'name': _('stock'),
         'type': 'ir.actions.act_window',
         'view_mode': 'form',
         'view_type': 'form',
         'res_model': model,
         'context': ctx,
         'target': 'new', 
         }        
