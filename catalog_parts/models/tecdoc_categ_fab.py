# -*- coding: utf-8 -*-

from odoo import api, fields, models, _ 


class article_fabricant(models.Model):
    _name = 'product.tecdoc'
    
    supplier = fields.Char('Supplier')
    supplier_id = fields.Char('Supplier id')
    art_id = fields.Char('art id')
    ref_article = fields.Char('Reference')
    designation = fields.Char('Designation')
    tab_id = fields.Many2one('search.fabricant', ondelete='cascade')
    marque = fields.Char('Brand')
    @api.multi
    def stock_tec(self):
        ctx = self._context.copy()
        active_id = self.id
        print ('----active_id:',active_id)
        ctx.update({'default_default_code': self.ref_article,'default_article_id': self.art_id,'default_supplier': self.supplier,'default_designation': self.designation,'default_supplier_id': self.supplier_id,'default_name': self.designation})
        view_id = self.env.ref('product.product_normal_form_view').id
        model = 'product.product'
        return {
        'name': _('stock'),
        'type': 'ir.actions.act_window',
        'view_mode': 'form',
        'view_type': 'form',
        'res_model': model,
        'context': ctx,
        'target': 'new', 
        }       
