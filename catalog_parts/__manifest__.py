# -*- coding: utf-8 -*-
{
    'name': "catalog of parts",
    "____________"

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Roua Cherif & Marwa Gharred",
    'website': "http://www.yourcompany.com",

    'category': 'Tools',
    'version': '12.1',

    'depends': ['base','sale', 'stock'],

#*******
    'data': [
         'security/ir.model.access.csv',
         'wizard/wizard.xml',
         'wizard/wizard_marque_categorie_tecdoc.xml',
         'wizard/wizard_categorie_tecdoc.xml',
         'wizard/categorie_marque_stock.xml',
         'wizard/wizard_categorie_stock.xml',
         'wizard/wizard_articles_stock.xml',
         'wizard/wizard_articles_tecdoc.xml',
         'views/search_by_reference.xml',
         'views/search_by_supplier.xml',
         'views/product.xml',
         'views/connexion_server.xml',
         'views/sale_view.xml',
         'views/search_by_vehicule_view.xml',
         'views/res_partner.xml',
         'views/equiv_stock_form.xml',
         'views/menu.xml',
         'data/data.xml',
         
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
